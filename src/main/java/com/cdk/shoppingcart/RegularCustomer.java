package com.cdk.shoppingcart;

import java.util.ArrayList;

public class RegularCustomer extends BaseCustomer  {
	
	public RegularCustomer() {
		slabList = new ArrayList<>();
		slabList.add(new Discount(0, 5000, 0));
		slabList.add(new Discount(5000, 10000, 10));
		slabList.add(new Discount(10000, Long.MAX_VALUE, 20));
	}

}
