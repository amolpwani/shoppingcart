package com.cdk.shoppingcart;

import java.util.List;

public abstract class BaseCustomer {
	protected List<Discount> slabList;
	
	class Discount {
		double min;
		double max;
		double perDiscount;
		
		public Discount(long min, long max, double discount) {
			this.min = min;
			this.max= max;
			this.perDiscount = discount;
		}
	}
	
	public double getBillAmount(double purchaseAmount) {
		double billAmount = 0;
		
		for (Discount discountObj : slabList) {
			if (purchaseAmount <= discountObj.max)
				billAmount += getSlabDiscountedAmount(discountObj, purchaseAmount - discountObj.min);
			else
				billAmount += getSlabDiscountedAmount(discountObj, (discountObj.max - discountObj.min));
			
			if (purchaseAmount <= discountObj.max) {
				break;
			}
		}
		
		return billAmount;
	}
	
	private double getSlabDiscountedAmount(Discount discountObj, double amount) {
		double discount = discountObj.perDiscount / 100f;
		return amount - (amount * (discount));
	}
}
