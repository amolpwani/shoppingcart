package com.cdk.shoppingcart;

public class ShoppingCart {
	public double getBillingAmount(CustomerType customerType, double purchaseAmount) {
		CustomerFactory customerFactory = new CustomerFactory();
		BaseCustomer baseCustomer = customerFactory.getCustomer(customerType);
		return baseCustomer.getBillAmount(purchaseAmount);
	}
	
	public static void main(String args []) {
		ShoppingCart shoppingCart = new ShoppingCart();
		System.out.println(shoppingCart.getBillingAmount(CustomerType.REGULAR, 5001f));
	}
}
