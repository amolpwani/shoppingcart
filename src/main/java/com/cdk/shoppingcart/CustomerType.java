package com.cdk.shoppingcart;

public enum CustomerType {
	
	REGULAR,
	PREMIUM,
	GOLD,
	PLATINUM,
	DAIMOND
}
