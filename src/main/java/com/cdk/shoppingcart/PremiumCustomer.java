package com.cdk.shoppingcart;

import java.util.ArrayList;

public class PremiumCustomer extends BaseCustomer  {
	
	public PremiumCustomer() {
		slabList = new ArrayList<>();
		slabList.add(new Discount(0, 4000, 10));
		slabList.add(new Discount(4000, 8000, 15));
		slabList.add(new Discount(8000, 12000, 20));
		slabList.add(new Discount(12000, Long.MAX_VALUE, 30));
	}

}
