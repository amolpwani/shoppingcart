package com.cdk.shoppingcart;

public class CustomerFactory {
	   public BaseCustomer getCustomer(CustomerType customerType){
	      switch (customerType) {
	      	case REGULAR :
	      		return new RegularCustomer();
	      	case PREMIUM :
	      		return new PremiumCustomer();
	      	default :
	      		return null;
	      }
	   }
}
