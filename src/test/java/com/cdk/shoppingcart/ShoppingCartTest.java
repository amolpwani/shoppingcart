package com.cdk.shoppingcart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShoppingCartTest {
	
	@Test
	public void testRegularCustomer() {
		ShoppingCart shoppingCart = new ShoppingCart();
		assertEquals(5000.0, shoppingCart.getBillingAmount(CustomerType.REGULAR, 5000.0), 0);
		assertEquals(9500.0, shoppingCart.getBillingAmount(CustomerType.REGULAR, 10000.0), 0);
		assertEquals(13500.0, shoppingCart.getBillingAmount(CustomerType.REGULAR, 15000.0), 0);
	}
	
	@Test
	public void testPreminumCustomer() {
		ShoppingCart shoppingCart = new ShoppingCart();
		assertEquals(3600.0, shoppingCart.getBillingAmount(CustomerType.PREMIUM, 4000.0), 0);
		assertEquals(7000.0, shoppingCart.getBillingAmount(CustomerType.PREMIUM, 8000.0), 0);
		assertEquals(10200.0, shoppingCart.getBillingAmount(CustomerType.PREMIUM, 12000.0), 0);
		assertEquals(15800.0, shoppingCart.getBillingAmount(CustomerType.PREMIUM, 20000.0), 0);
	}

}
